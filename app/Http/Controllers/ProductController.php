<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        try {
            $products = Product::all();
        
            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'data' => ([
                    'products' => $products
                ])
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Fetching All Data'
            ], 400);
        }
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            "title" => "required|unique:product",
            "price" => "required"
        ]);
        
        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'code' => env('STATUS_FAILED_CODE'),
                'message' => $errorString
            ], 400);
        }
        
        try {
            $product = Product::create($data);

            if ($product) {
                return response()->json([
                    'status' => env('STATUS_SUCCESS_TEXT'),
                    'data' => ([
                        'product' => $product
                    ])
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'code' => env('STATUS_FAILED_CODE'),
                'message' => 'Error While Create Product'
            ], 400);
        }   
    }

    public function show($id)
    {
        try {
            $product = Product::find($id);
            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'data' => ([
                    'product' => $product])
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Showing The Product'
            ], 400);
        }
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        
        if (!$product) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Data not found'
            ], 404);
        }
        
        $validator = Validator::make($request->all(), [
            "title" => "required|unique:product",
            "price" => "required"
        ]);
        
        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $data = $request->all();
        $product->fill($data);
        
        try {
            $product->save();

            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'data' => ([
                    'product' => $product
                ])
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Updating Data'
            ], 400);
        }
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        
        if (!$product) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Data not found'
            ], 404);
        }

        try {
            $product->delete();
            return response()->json([
                'status' =>  env('STATUS_SUCCESS_TEXT'),
                'message' => 'Successfully Deleting Data'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Deleting Data'
            ], 400);
        }
    }
} 