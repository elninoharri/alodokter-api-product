# Lumen API Alodokter Product

API with Lumen 8.

### Installation Using Docker

- Clone the Repo:
    - `git clone git@gitlab.com:elninoharri/alodokter-api-product.git`
    - `git clone https://gitlab.com/elninoharri/alodokter-api-product.git`
- `cd alodokter-api-product`
- SSH into the Docker container with `make ssh` and run the following.
    - `composer create-project`
    - `php artisan migrate`
- Exit from Docker container with `CTRL+C` or `exit`.
- Rename `docker-compose.local.yaml` to `docker-compose.overridee.yaml`
- Start the local development server with `make up`.
- Run tests with `make dev-test`.
- Run `make` to see available commands.
- Always `ssh` into Docker container `app` by running `make ssh` before executing any `artisan` commands.

### Installation On Local
- Clone the Repo:
    - `git clone git@gitlab.com:elninoharri/alodokter-api-product.git`
    - `git clone https://gitlab.com/elninoharri/alodokter-api-product.git`
- `cd alodokter-api-product`
- On Visual Studio Code Terminal run the following.
    - `composer install`
    - `php artisan migrate`
    - `php -S localhost:8800 -t public`