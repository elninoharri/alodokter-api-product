<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api/v1'], function ($router) { 
    $router->post('/create','ProductController@create');
    $router->get('/lists','ProductController@index');
    $router->get('/detail/{id}','ProductController@show');
    $router->put('/update/{id}','ProductController@update');
    $router->delete('/delete/{id}','ProductController@destroy');
});
